import urllib3
from elasticsearch import Elasticsearch
import requests
from datetime import datetime
from os import environ

dia = datetime.now().isoformat() 

usuario = environ.get('USUARIO')
senha = environ.get('SENHA')
es_host = environ.get('ES_HOST')

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

url = 'https://www.waze.com/row-rtserver/web/TGeoRSS?bottom=-3.179700819552629&left=-60.37982940673829&ma=200&mj=100&mu=20&right=-59.58881378173829&top=-2.9110532097493453&types=alerts,traffic,users'
headers = {'Referer': 'https://www.waze.com/pt-BR/live-map'}

request = requests.get(url, headers=headers, verify=False)
Rjson = request.json()
users = Rjson['users']

for user in users:
    user['dia_coleta'] = str(dia)

try:
    es = Elasticsearch(
        [es_host],  
        http_auth=(usuario, senha), 
        scheme="https",  
        port=9300, 
        verify_certs=False
    )
except:
    print('failed to connect to Elasticsearch')
es.indices.create(index='index_users', ignore=400)
for user in users:
    response = es.index(index='index_users', id=user['id'], body=user)