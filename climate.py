import requests
import urllib3
from elasticsearch import Elasticsearch
from os import environ
from datetime import datetime, date, time
import pytz

timezone = pytz.timezone("America/Manaus")

usuario = environ.get('USUARIO')
senha = environ.get('SENHA')
es_host = environ.get('ES_HOST')

tempus = datetime.now()

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

url = 'https://node.windy.com/pois/v2/stations/-3.132/-59.983?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTU5NjU4MDQsImluZiI6eyJpcCI6IjIwMC4yNDIuNDMuMjAyIiwidWEiOiJNb3ppbGxhXC81LjAgKFgxMTsgTGludXggeDg2XzY0KSBBcHBsZVdlYktpdFwvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lXC8xMjAuMC4wLjAgU2FmYXJpXC81MzcuMzYifSwiZXhwIjoxNzE2MTM4NjA0fQ.BgyijSz2GlkjWYE8RI6JsjxOjccN0kS1CS4zzCgagX0&token2=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtYWdpYyI6MzAzLCJpYXQiOjE3MTU5NjU4MDUsImV4cCI6MTcxNjEzODYwNX0.g8mq5sKoxQ68MJDU3G8cc3kg-ZiNbhhYmG2W_fHMR7A&uid=fcc1ac89-bf80-fc32-5261-e8e52a9d7faf&sc=9&pr=0&v=42.1.6&poc=47'
headers = {'Referer':'https://www.windy.com/'}

response = requests.get(url, headers=headers, verify=False).json()

def AproxTime(data):

    now = datetime.now().astimezone(timezone).isoformat()

    hour = data['hAgo']
    minute = data['minAgo']

    if data['hAgo'] == 'None':
        hour = 0
    elif data['minAgo'] == 'None':
        minute = 0

    hm = time(hour, minute)

    comb = datetime.combine(date.today(), hm)
    hmiso = comb.astimezone(timezone).isoformat()

    atual = datetime.fromisoformat(now)
    estacao = datetime.fromisoformat(hmiso)

    diff = atual - estacao

    timeref = datetime.combine(datetime.now().date(), datetime.min.time())

    aprox = timeref + diff
    aprox = aprox.astimezone(timezone).isoformat()

    return aprox

for data in response:
    data['id_estacao'] = data['id']
    data['id'] = str(data['id']) + '-' + str(tempus.day) + str(tempus.month) + str(tempus.year) + '-' + str(tempus.hour) + str(tempus.minute) 
    data['date_coleta'] = str(tempus.astimezone(timezone).isoformat())
    data['location'] = str(data['lat']) + ',' + str(data['lon'])
    
    pxtime = AproxTime(data)

    data['hora_aproximada'] = str(pxtime)

try:
    es = Elasticsearch(
        [es_host],  
        http_auth=(usuario, senha), 
        scheme="https",  
        port=9300, 
        verify_certs=False
    )
except:
    print('failed to connect to Elasticsearch')
es.indices.create(index='index_clima', ignore=400)
for data in response:
    response = es.index(index='index_clima', id=data['id'], body=data)