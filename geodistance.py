from elasticsearch import Elasticsearch
from datetime import datetime, timedelta
import urllib3
from os import environ

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

usuario = environ.get('USUARIO')
senha = environ.get('SENHA')
es_host = environ.get('ES_HOST')

es = Elasticsearch(
        [es_host],  
        http_auth=(usuario, senha), 
        scheme="https",  
        port=9300, 
        verify_certs=False)

index_jams = 'index_jams'
index_climate = 'index_clima'

def getDateRange(date):
    
    dt = datetime.fromisoformat(date)

    start = dt - timedelta(minutes=30)
    ends = dt + timedelta(minutes=30)

    return start, ends

def InsertWeather(alert, weather):

  doc = alert['_source']

  doc['clima_precipitacao'] = weather['_source']['precip']
  doc['clima_temperatura(°C)'] = weather['_source']['temp']
  doc['clima_ponto_de_orvalho'] = weather['_source']['dew_point']
  doc['clima_umidade_relativa'] = weather['_source']['rh']
  doc['clima_estacao_coletada'] = weather['_source']['id']
  doc['clima_timestamp'] = weather['_source']['hora_aproximada']
  doc['clima_field'] = True

  return doc

def getGeoDistance(data, distance):
    
    start, ends = getDateRange(data['_source']['date_alerta'])

    query_geodis = {
      "size": 1,
      "query": {
        "bool": {
          "must": {
            "match_all": {}
          },
          "filter": [
            {
              "geo_distance": {
                "distance": str(distance) + "km",
                "location": data['_source']['location'] 
              }
            },
            {
              "range": {
                "hora_aproximada": {
                  "gte": start, 
                  "lte": ends  
                }
              }
            }
          ]
        }
      },
      "sort":[
         {
            "_geo_distance":{
               "location":data['_source']['location'],
               "order": "asc",
               "unit":"km",
               "mode":"min",
               "distance_type":"arc"
          }
        }
      ]
    }

    geosearch = es.search(index=index_climate, body=query_geodis)

    return geosearch

def Index_matchData(init_search):
  for data in init_search['hits']['hits']:
  
    distrange = 100
    geosearch = getGeoDistance(data, distrange)

    while geosearch['hits']['total']['value'] == 0:
      if distrange >= 400:
        skip_data = True
        break
      else:
        distrange = distrange * 2
        geosearch = getGeoDistance(data, distrange)
    
    if 'skip_data' in locals():
      del skip_data
      continue
        
    for hit in geosearch['hits']['hits']:
        document = InsertWeather(data, hit)
        document_id = data['_id']
    response = es.update(index='francisco_waze_alerts', id=document_id, body={"doc": document })
    
# primeira query, para coletar alerts
query_alerts = {
  "_source": ["location", "dia_coleta", "clima_field"],
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "location_field": True
          }
        },
        {
          "match": {
            "clima_field": False
          }
        }
      ]
    }
  }
}

scroll_timeout = '5m'

init_search = es.search(index=index_jams, body=query_alerts, scroll=scroll_timeout, size=1000)

Index_matchData(init_search)

scroll_id = init_search['_scroll_id'] 
scroll_size = init_search['hits']['total']['value'] 

while scroll_size > 0:

  scroll_results = es.scroll(scroll_id=scroll_id, scroll=scroll_timeout)
  scroll_id = scroll_results['_scroll_id']
  scroll_size = len(scroll_results['hits']['hits'])
  
  Index_matchData(init_search)

es.clear_scroll(scroll_id=scroll_id)