import urllib3
from elasticsearch import Elasticsearch
import requests
from datetime import datetime
from os import environ
import pytz

# configurações
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

tempus = datetime.now()

usuario = environ.get('USUARIO')
senha = environ.get('SENHA')
es_host = environ.get('ES_HOST')

timezone = pytz.timezone("America/Manaus")

url = 'https://brasilapi.com.br/api/feriados/v1/' + str(tempus.year)

# listas com feriados municipais e estaduais
municipal = [
    {'data_feriado': str(tempus.year) + '-10-24', 'feriado': 'Aniversário de Manaus', 'tipo': 'municipal'},
    ]

estadual = [
    {'data_feriado': str(tempus.year) + '-09-05', 'feriado': 'Elevação do Amazonas à Categoria de Pronvíncia', 'tipo': 'estadual'},
    {'data_feriado': str(tempus.year) + '-12-08', 'feriado': 'Dia de Nossa Senhora da Conceição, Padroeira do Amazonas', 'tipo': 'estadual'}
    ]

# função para retornar dia da semana do feriado
def getSemana(data):

    dt = datetime.strptime(data['data_feriado'], "%Y-%m-%d")

    week = dt.weekday()
    ext = ["Segunda-Feira", "Terça-Feira", "Quarta-Feira", "Quinta-Feira", "Sexta-Feira", "Sábado", "Domingo"]
    dia_semana = ext[week]

    data['dia_semana'] = dia_semana

    tz = dt.astimezone(timezone)

    data['data_feriado'] = tz.isoformat()
    
    return data

# requisição
re = requests.get(url).json()

# iteração e tratamento dos dados
for data in re:
    data['data_feriado'] = data.pop('date')
    data['feriado'] = data.pop('name')
    data['tipo'] = data.pop('type')

    if data['tipo'] == 'national':
        data['tipo'] = 'nacional'

# soma do json raiz + feriados municipais e estaduais
re = re + municipal + estadual

# conexão com Elastic e envio dos Dados
try:
    es = Elasticsearch(
        [es_host],  
        http_auth=(usuario, senha), 
        scheme="https",  
        port=9300, 
        verify_certs=False
    )
except:
    print('failed to connect to Elasticsearch')
es.indices.create(index='index_feriados', ignore=400)
for data in re:

    # chama função do dia da semana
    data = getSemana(data)

    # define id do dado
    data['id'] = 'id-' + data['data_feriado']

    # envia pro elastic
    response = es.index(index='index_feriados', id=data['id'], body=data)